﻿using FluentAssertions;
using LW6.Helpers;
using LW6.PageObjects;
using Microsoft.Playwright;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LW6.Tests
{
    public class DemoblazePlaywrightTests
    {
        private const string ResultsFolderPath = "results";
        private readonly string TraceFolderPath = $"{ResultsFolderPath}/traces";
        private readonly string VideosFolderPath = $"{ResultsFolderPath}/videos";
        private readonly string FailedTestsVideoFolderPath = $"{ResultsFolderPath}/videos/fail";

        private IPlaywright _playwright;
        private IBrowser _browser;
        private IBrowserContext _context;
        private IPage _page;

        [OneTimeSetUp]
        public async Task OneTimeSetUp()
        {
            if (Directory.Exists(ResultsFolderPath))
            {
                Directory.Delete(ResultsFolderPath, true);
            }

            _playwright = await Playwright.CreateAsync();
            _browser = await _playwright.Chromium.LaunchAsync(new BrowserTypeLaunchOptions
            {
                
            });
        }

        [SetUp]
        public async Task SetUp()
        {
            _context = await _browser.NewContextAsync(new BrowserNewContextOptions
            {
                RecordVideoDir = VideosFolderPath
            });

            await _context.Tracing.StartAsync(new TracingStartOptions
            {
                Name = TestContext.CurrentContext.Test.Name,
                Screenshots = true,
                Snapshots = true
            });

            _page = await _context.NewPageAsync();
        }

        [TearDown]
        public async Task TearDown()
        {
            await _page.CloseAsync();

            TracingStopOptions stopTraceOptions = null;
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                stopTraceOptions = new TracingStopOptions
                {
                    Path = $"{TraceFolderPath}/{TestContext.CurrentContext.Test.Name}.zip"
                };

                await _page.Video.SaveAsAsync($"{FailedTestsVideoFolderPath}/{TestContext.CurrentContext.Test.Name}.webm");
            }

            await _page.Video.DeleteAsync();

            await _context.Tracing.StopAsync(stopTraceOptions);
            await _context.CloseAsync();
            await _context.DisposeAsync();
        }

        [OneTimeTearDown]
        public async Task OneTimeTearDown()
        {
            _playwright.Dispose();
            await _browser.DisposeAsync();
        }

        [Test]
        public async Task SignUp_Successful()
        {
            //arrange
            string seed = Guid.NewGuid().ToString().Substring(0, 6);
            string login = $"andriy.dovgalyuk.{seed}";
            string password = "andriy.dovgalyuk";
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            string dialogMessage = null;

            //act 
            _page.Dialog += async (_, dialog) =>
            {
                dialogMessage = dialog.Message;
                await dialog.AcceptAsync();
                tokenSource.Cancel();
            };

            var mainPage = new MainPage(_page);
            await mainPage.GotoAsync();
            await mainPage.OpenSignUpFormAsync();

            var signUpForm = new SignUpForm(_page);
            await signUpForm.FillLoginAsync(login);
            await signUpForm.FillPasswordAsync(password);
            await signUpForm.SignUpAsync();

            await DialogHelper.WaitForDialog(tokenSource.Token);

            //assert
            await _page.WaitForSelectorAsync(SignUpForm.Selector, new PageWaitForSelectorOptions { State = WaitForSelectorState.Hidden });
            var isFormVisible = await signUpForm.IsVisibleAsync();

            _page.Url.Should().Be(MainPage.URL);
            dialogMessage.Should().Be(SignUpForm.SuccessfulSignUpMessage);
            isFormVisible.Should().BeFalse();
        }

        [Test]
        public async Task SignUp_Failed()
        {
            //arrange
            string login = "andriy.dovgalyuk";
            string password = "andriy.dovgalyuk";
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            string dialogMessage = null;

            //act
            _page.Dialog += async (_, dialog) =>
            {
                dialogMessage = dialog.Message;
                await dialog.AcceptAsync();
                tokenSource.Cancel();
            };

            var mainPage = new MainPage(_page);
            await mainPage.GotoAsync();
            await mainPage.OpenSignUpFormAsync();

            var signUpForm = new SignUpForm(_page);
            await signUpForm.FillLoginAsync(login);
            await signUpForm.FillPasswordAsync(password);
            await signUpForm.SignUpAsync();

            await DialogHelper.WaitForDialog(tokenSource.Token);

            //assert

            await _page.WaitForSelectorAsync(SignUpForm.Selector, new PageWaitForSelectorOptions { State = WaitForSelectorState.Visible });
            var isFormVisible = await signUpForm.IsVisibleAsync();

            _page.Url.Should().Be(MainPage.URL);
            dialogMessage.Should().Be(SignUpForm.UserExistsSignUpMessage);
            isFormVisible.Should().BeTrue();
        }

        [Test]
        public async Task LogIn_Successful()
        {
            //arrange
            string login = "andriy.dovgalyuk";
            string password = "andriy.dovgalyuk";

            //act 
            var mainPage = new MainPage(_page);
            await mainPage.GotoAsync();
            await mainPage.OpenLoginFormAsync();

            var loginForm = new LoginForm(_page);

            await loginForm.FillLoginAsync(login);
            await loginForm.FillPasswordAsync(password);

            await loginForm.LoginAsync();

            var mainPageAfterLogin = new MainPageLogined(_page);
            await _page.WaitForSelectorAsync(mainPageAfterLogin.UsernameSelector, new PageWaitForSelectorOptions() { State = WaitForSelectorState.Visible });

            //assert
            string username = await mainPageAfterLogin.GetUsernameAsync();
            username.Should().Contain(login);
            _page.Url.Should().Be(MainPage.URL);

            bool isLoginFormVisible = await loginForm.IsVisibleAsync();
            isLoginFormVisible.Should().Be(false);
        }

        [Test]
        public async Task LogIn_Failed_UserDoesntExist()
        {
            //arrange
            string seed = Guid.NewGuid().ToString();
            string login = seed.Substring(0, 5);
            string password = seed.Substring(5, 5);
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            string dialogMessage = null;

            //act 
            _page.Dialog += async (_, dialog) =>
            {
                dialogMessage = dialog.Message;
                await dialog.AcceptAsync();
                tokenSource.Cancel();
            };

            var mainPage = new MainPage(_page);
            await mainPage.GotoAsync();
            await mainPage.OpenLoginFormAsync();

            var loginForm = new LoginForm(_page);
            await loginForm.FillLoginAsync(login);
            await loginForm.FillPasswordAsync(password);
            await loginForm.LoginAsync();

            await DialogHelper.WaitForDialog(tokenSource.Token);

            //assert
            _page.Url.Should().Be(MainPage.URL);

            await _page.WaitForSelectorAsync(LoginForm.Selector, new PageWaitForSelectorOptions { State = WaitForSelectorState.Visible });
            dialogMessage.Should().Be(LoginForm.UserDoesntExistLoginMessage);
            bool isLoginFormVisible = await loginForm.IsVisibleAsync();
            isLoginFormVisible.Should().Be(true);
        }

        [Test]
        public async Task LogIn_Failed_WrongPassword()
        {
            //arrange
            string seed = Guid.NewGuid().ToString();
            string login = "andriy.dovgalyuk";
            string password = seed.Substring(0, 5);
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            string dialogMessage = null;

            //act 
            _page.Dialog += async (_, dialog) =>
            {
                dialogMessage = dialog.Message;
                await dialog.AcceptAsync();
                tokenSource.Cancel();
            };

            var mainPage = new MainPage(_page);
            await mainPage.GotoAsync();
            await mainPage.OpenLoginFormAsync();

            var loginForm = new LoginForm(_page);
            await loginForm.FillLoginAsync(login);
            await loginForm.FillPasswordAsync(password);
            await loginForm.LoginAsync();

            await DialogHelper.WaitForDialog(tokenSource.Token);

            //assert
            _page.Url.Should().Be(MainPage.URL);

            await _page.WaitForSelectorAsync(LoginForm.Selector, new PageWaitForSelectorOptions { State = WaitForSelectorState.Visible });
            dialogMessage.Should().Be(LoginForm.WrongPasswordLoginMessage);
            bool isLoginFormVisible = await loginForm.IsVisibleAsync();
            isLoginFormVisible.Should().Be(true);
        }
    }
}
