﻿using Microsoft.Playwright;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW6.PageObjects
{
    public abstract class PageObjectBase
    {
        protected readonly IPage _page;

        public PageObjectBase(IPage page)
        {
            _page = page;
        }
    }
}
